#include <sys/types.h>

#include <net/if.h>
#include <arpa/inet.h>

#include <string.h>

#include <unistd.h>

#include <stdio.h>


class Ip 
{
#define INET_ADDR_LEN sizeof(in_addr)
private:
    u_int8_t address[INET_ADDR_LEN];
    char str[INET_ADDRSTRLEN];
public:
    u_int8_t *get_address() 
    {
        return address;
    }

    char *to_string()
    {
        inet_ntop(AF_INET, address, str, sizeof(str));
        return str;
    }

    int operator ==(u_int8_t *ip)
    {
        return !memcmp(address, ip, INET_ADDR_LEN);
    }

    int operator =(u_int8_t *ip)
    {
        return !memcpy(address, ip, INET_ADDR_LEN);
    }


    int operator =(char *ip)
    {
      return inet_pton(AF_INET, ip, address);
    }
};