all : arp_spoof

arp_spoof: main.o arp_spoof.o
	g++ -g -o arp_spoof main.o arp_spoof.o send_arp.o -lpcap -pthread

send_arp.o: send_arp.h
	g++ -g -c -o send_arp.o send_arp.cpp

ip.o:
	g++ -g -c -o ip.o ip.cpp

mac.o:
	g++ -g -c -o mac.o mac.cpp

arp_spoof.o: send_arp.o ip.o mac.o
	g++ -g -c -o arp_spoof.o arp_spoof.cpp

main.o: send_arp.h
	g++ -g -c -o main.o main.cpp

clean:
	rm -f arp_spoof
	rm -f *.o

