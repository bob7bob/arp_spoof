#include "arp_spoof.h"

void usage() {
    printf("syntax: arp_spoof <interface> <sender ip 1> <target ip 1> [<sender ip 2> <target ip 2>...]\n");
    printf("sample: arp_spoof wlan0 192.168.10.2 192.168.10.1 192.168.10.1 192.168.10.2\n");
}

int main(int argc, char* argv[]) {
    if (argc < 4) {
      usage();
      return -1;
    }
    setvbuf(stdout, NULL, _IONBF, 0);
    const char *dev = argv[1];
    
    ArpSpoof arp_spoof = ArpSpoof(dev);
    arp_spoof.set_sessions((argc - 2) / 2, &argv[2]);
    arp_spoof.arp_spoof();
    return 0;
}

/*
[리포트]
arp spoofing 프로그램을 구현하라.

[프로그램]
arp_spoof <interface> <sender ip 1> <target ip 1> [<sender ip 2> <target ip 2>...]
ex : arp_spoof wlan0 192.168.10.2 192.168.10.1 192.168.10.1 192.168.10.2

[상세]
이전 과제(send_arp)를 다 수행하고 나서 이번 과제를 할 것.

오늘 배운 "ARP spoofing의 모든 것" PPT 숙지할 것.

코드에 victim, gateway라는 용어를 사용하지 말고 반드시 sender, target(혹은 receiver)라는 단어를 사용할 것.

sender에서 보내는 spoofed IP packet을 attacker가 수신하면 이를 relay하는 것 코드 구현.

sender에서 infection이 풀리는 시점을 정확히 파악하여 재감염시키는 코드 구현.

주기적으로 ARP injection packet을 날리는 코드 구현.

코딩 능력이 된다면 (sender, target) 세션을 여러개 처리할 수 있도록 코드 구현.

[리포트 제목]
char track[] = "개발"; // "취약점", "컨설팅", "포렌식"
char name[] = "홍길동";
printf("[bob7][%s]arp_spoof[%s]", track, name);

[제출 기한]
2018.08.13 23:59

[ps]
소스 코드는 가급적 C, C++(다른 programming language에 익숙하다면 그것으로 해도 무방).

bob@gilgil.net 계정으로 자신의 git repository 주소를 알려 줄 것.

절대 KITRI access point 네트워크를 대상으로 테스트하지 말 것. 하려면 핫스팟을 띄워 하거나 BoBDev 라는 access point를 사용할 것.

본 과제는 어려운 레벨에 속하므로 충분한 시간을 투자하여 구현할 것(마감 하루 이틀 전에 과제 시작하지 말고).
*/