#include <sys/types.h>

#include <string.h>

#include <unistd.h>

#ifdef __MACH__
    #include <net/ethernet.h>
    #include <netinet/if_ether.h>
#else
    #include <netinet/ether.h>
    #include <netinet/if_ether.h>
    #include <arpa/inet.h>
#endif


class Mac
{

#define ETHER_ADDRSTRLEN ETHER_ADDR_LEN * 3 + 1

private:
    u_int8_t address[ETHER_ADDR_LEN];
    char str[ETHER_ADDRSTRLEN];
public:
    u_int8_t *get_address() 
    {
        return address;
    }
    
    char *to_string()
    {
        strncpy(str, ether_ntoa((struct ether_addr*)address), sizeof(str));
        return str;
    }

    int operator ==(u_int8_t *mac)
    {
        return !memcmp(address, mac, ETHER_ADDR_LEN);
    }

    int operator =(u_int8_t *mac)
    {
        return !memcpy(address, mac, ETHER_ADDR_LEN);
    }

};