#include "send_arp.h"
#include "mac.h"
#include "ip.h"

#include <list>

using namespace std;

class ArpSpoof : public SendArp 
{
    struct Session
    {
        Ip sender_ip;
        Mac sender_mac;
        Ip target_ip;
        Mac target_mac;
    };
    list<Session *> sessions_;
    
private:
    int get_sessions_mac();
    int relay_packet(u_int8_t *data, u_int8_t *dst_mac, size_t len);
public:
    ArpSpoof(const char *dev) : SendArp(dev)
    {

    }
    list<Session *>& get_sessions() { return sessions_; }
    int set_sessions(int len, char **argv);

    int arp_spoof();
    int relay();
   
    
    

};
void *relay_thread(void *);
